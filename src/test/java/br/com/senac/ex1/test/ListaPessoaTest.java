
package br.com.senac.ex1.test;

import br.com.senac.ex1.ListaPessoa;
import br.com.senac.ex1.modelo.Pessoa;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ListaPessoaTest {
    
    public ListaPessoaTest() {
    }
    
    
    @Test
    public void joaoDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("João", 10);
        Pessoa miguel = new Pessoa("Miguel", 15);
        Pessoa malaquias = new Pessoa("Malaquias", 20);
        lista.add(joao);
        lista.add(miguel);
        lista.add(malaquias);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        
        assertEquals(joao, pessoaMaisNova);
        
        
        
    }
    
    
    @Test
    public void miguelNaoDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("João", 10);
        Pessoa miguel = new Pessoa("Miguel", 15);
        Pessoa malaquias = new Pessoa("Malaquias", 20);
        lista.add(joao);
        lista.add(miguel);
        lista.add(malaquias);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        
        assertNotEquals(miguel, pessoaMaisNova);
        
        
        
    }
    
    @Test
    public void joaoDeveSerMaisVelho(){
         List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("João", 100);
        Pessoa miguel = new Pessoa("Miguel", 15);
        Pessoa malaquias = new Pessoa("Malaquias", 20);
        lista.add(joao);
        lista.add(miguel);
        lista.add(malaquias);
        
        Pessoa pessoaMaisVelha = ListaPessoa.getPessoaMaisVelha(lista) ; 
        
        assertEquals(joao,pessoaMaisVelha);
        
        
    }
    
     @Test
    public void miguelNaoDeveSerMaisVelho(){
         List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("João", 100);
        Pessoa miguel = new Pessoa("Miguel", 15);
        Pessoa malaquias = new Pessoa("Malaquias", 20);
        lista.add(joao);
        lista.add(miguel);
        lista.add(malaquias);
        
        Pessoa pessoaMaisVelha = ListaPessoa.getPessoaMaisVelha(lista) ; 
        
        assertNotEquals(miguel,pessoaMaisVelha);
        
        
    }
    
    
    
    
    
}
